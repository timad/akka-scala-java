package remote;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import com.typesafe.config.ConfigFactory;

public class HelloJava {
  public static void main(String[] args) {
    final ActorSystem system = ActorSystem.create("HelloJava",
        ConfigFactory.load("hellojava"));
    final ActorRef actor = system.actorOf(Props.create(remote.HelloRemote.class),
        "helloJava");
    System.out.println(actor.path().toString());
    System.out.println("Started HelloJava");
  }
}
