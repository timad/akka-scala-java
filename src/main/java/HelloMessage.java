package remote;

import java.io.*;
import java.util.*;

public class HelloMessage implements Serializable {
  public String message;

  HelloMessage(String text) {
    message = text;
  }

  String getMessage() {
    return message;
  }
}

