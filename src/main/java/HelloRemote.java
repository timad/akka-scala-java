package remote;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class HelloRemote extends UntypedActor {

  @Override
  public void onReceive(Object message) throws Exception {

    if (message instanceof String) {
      System.out.printf("Java received string - %s\n", message);
    } else if (message instanceof HelloMessage) {
      System.out.printf("Java received JavaMessage - %s\n", ((HelloMessage)message).getMessage());
      this.getContext().actorSelection("akka.tcp://HelloScala@127.0.0.1:2552/user/helloScala")
              .tell(remote.ScalaMessage$.MODULE$.apply("answer from java"), getSelf());
    } else {
      unhandled(message);
    }
  }
}
