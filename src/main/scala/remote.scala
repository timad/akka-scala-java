package remote

import akka.actor._
import com.typesafe.config.ConfigFactory

case class ScalaMessage(text: String)

class ScalaActor extends Actor {
  def receive = {
    case msg: ScalaMessage => println(msg.text)
    case _ => println("unhandled")
  }
}

object HelloScala extends App  {
  val system = ActorSystem("HelloScala", ConfigFactory.load("helloscala"))
  val local = system.actorOf(Props[ScalaActor], "helloScala")
  val remoteActor = system.actorSelection("akka.tcp://HelloJava@127.0.0.1:2554/user/helloJava")
  remoteActor ! new HelloMessage("Message from scala")
}
